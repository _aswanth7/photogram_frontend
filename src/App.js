import React from "react";
import { Router, Switch, Route } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";

import "./App.css";
import Home from "./components/Home/Home";
import Login from "./components/Login/Login";
import Signup from "./components/Signup/Signup";
import history from "./util/history";
import ProtectedRoute from "./router/ProtectedRoute";
import { connect } from "react-redux";
import Profile from "./components/Profile/Profile";

class App extends React.Component {
  render() {
    return (
      <Router history={history}>
        <Switch>
          <Route path="/login" component={Login}></Route>
          <Route path="/signup" component={Signup}></Route>

          <ProtectedRoute
            path="/"
            exact
            component={Home}
            user={this.props.user.user}
          ></ProtectedRoute>
          {/* <ProtectedRoute
            path="/profile"
            component={Profile}
            user={this.props.user.user}
          ></ProtectedRoute> */}
          <Route path="/profile" component={Profile}></Route>
        </Switch>
      </Router>
    );
  }
}

const mapStateToProps = (user) => {
  return { user: user.login };
};
export default connect(mapStateToProps, null)(App);
