import axios from "axios";
import { Login_Type } from "./actionTypes";

const userLogin = (username, password) => async (dispatch) => {
  try {
    dispatch({ type: Login_Type.LOGIN_REQUEST });
    let { data } = await axios.post(
      process.env.REACT_APP_BACKEND_URL + "/auth/login",
      {
        username,
        password,
      },
      {
        headers: {
          "Content-Type": "application/json",
        },
      }
    );
    dispatch({
      type: Login_Type.LOGIN_SUCCESS,
      payload: data,
    });
    localStorage.setItem("userInfo", JSON.stringify(data));
  } catch (error) {
    console.log(error, "In login action");
    dispatch({
      type: Login_Type.LOGIN_FAILURE,
      payload: error.response.data.message,
    });
  }
};

const userLogout = () => (dispatch) => {
  localStorage.removeItem("userInfo");
  dispatch({ type: Login_Type.LOGOUT });
};
export default { userLogin, userLogout };
