import { Login_Type } from "../action/actionTypes";
// let userInfoStorage = localStorage.getItem("userInfo")
//   ? JSON.parse(localStorage.getItem("userInfo"))
//   : null;
let initialState = {
  loading: null,
  user: null,
  error: null,
};

const loginReducer = (state = initialState, action) => {
  switch (action.type) {
    case Login_Type.LOGIN_REQUEST:
      return { loading: true };
    case Login_Type.LOGIN_SUCCESS:
      return { loading: false, user: action.payload };
    case Login_Type.LOGIN_FAILURE:
      return { loading: false, error: action.payload };
    case Login_Type.LOGOUT:
      return { loading: false, user: null, error: null };
    default:
      return state;
  }
};

export default loginReducer;
