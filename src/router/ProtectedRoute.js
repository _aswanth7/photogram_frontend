import React from "react";
import { connect } from "react-redux";
import { Redirect, Route } from "react-router";

const ProtectedRoute = ({ path, component: Component, user, ...rest }) => {
  return (
    <Route
      path={path}
      render={() => {
        return user.user !== null ? (
          <Component {...rest}> </Component>
        ) : (
          <Redirect to="/login"></Redirect>
        );
      }}
    ></Route>
  );
};

const mapStateToProps = (user) => {
  return { user: user.login };
};
export default connect(mapStateToProps, null)(ProtectedRoute);
