import React, { Component } from "react";
import { Container, Row, Col, Form, Button } from "react-bootstrap";
import { Link, Redirect } from "react-router-dom";
import validator from "validator";
import axios from "axios";

import "./signup.css";
import history from "../../util/history";
import { connect } from "react-redux";

class Signup extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      email: "",
      phone: "",
      fullname: "",
      password: "",
      validationErrors: "",

      signupAPIError: null,
    };
  }

  submitFormData = async () => {
    try {
      await axios.post(`${process.env.REACT_APP_BACKEND_URL}/auth/signup`, {
        username: this.state.username,
        fullname: this.state.fullname,
        phone: this.state.phone,
        email: this.state.email,
        password: this.state.password,
      });
      this.setState({
        signupAPIError: null,
      });
      history.push("/login");
    } catch (error) {
      this.setState({
        signupAPIError: error.response.data.message,
      });
    }
  };

  formOnSubmitHandler = (event) => {
    event.preventDefault();
    if (!validator.isAlphanumeric(this.state.username)) {
      this.setState({
        validationErrors: "Usernames can only use letters, numbers.",
      });
    } else if (!validator.isEmail(this.state.email)) {
      this.setState({
        validationErrors: "Enter Valid Email",
      });
    } else if (!validator.isMobilePhone(this.state.phone, ["en-IN"])) {
      this.setState({
        validationErrors: "Enter Valid Phone Number",
      });
    } else if (!this.state.fullname.match(/^[A-Za-z ]+$/)) {
      this.setState({
        validationErrors: "Name can only user letters.",
      });
    } else if (!validator.isStrongPassword(this.state.password)) {
      this.setState({
        validationErrors:
          "Password must be 8 characters long, contain letters and numbers aleast 1 symbol , uppercase, lowercase. ",
      });
    } else {
      this.setState({
        validationErrors: "",
      });
      this.submitFormData();
    }
  };
  inputOnChangeHandler = (event) => {
    if (event.target.name === "username") {
      this.setState({
        username: event.target.value,
      });
    } else if (event.target.name === "password") {
      this.setState({
        password: event.target.value,
      });
    } else if (event.target.name === "email") {
      this.setState({
        email: event.target.value,
      });
    } else if (event.target.name === "phone") {
      this.setState({
        phone: event.target.value,
      });
    } else if (event.target.name === "fullname") {
      this.setState({
        fullname: event.target.value,
      });
    }
  };

  render() {
    if (this.props.user.user) {
      return <Redirect to="/" />;
    } else {
      return (
        <Container className="signup-body" fluid>
          <Row className="justify-content-center min-vh-100">
            <Col>
              <div className="signup-container">
                <div className="signup-heading">
                  <h1>Photogram</h1>
                </div>
                <Form onSubmit={this.formOnSubmitHandler}>
                  <Form.Group controlId="formBasicEmail">
                    <Form.Control
                      required
                      onChange={this.inputOnChangeHandler}
                      name="email"
                      type="email"
                      placeholder="Enter email"
                    />
                  </Form.Group>
                  <Form.Group controlId="formBasicPhone">
                    <Form.Control
                      required
                      onChange={this.inputOnChangeHandler}
                      name="phone"
                      type="text"
                      placeholder="Enter Phone"
                    />
                  </Form.Group>
                  <Form.Group controlId="formBasicName">
                    <Form.Control
                      required
                      onChange={this.inputOnChangeHandler}
                      name="fullname"
                      type="text"
                      placeholder="Enter Full Name"
                    />
                  </Form.Group>
                  <Form.Group controlId="formBasicUsername">
                    <Form.Control
                      required
                      onChange={this.inputOnChangeHandler}
                      name="username"
                      type="text"
                      placeholder="Enter username"
                    />
                  </Form.Group>
                  <Form.Group controlId="formBasicPassword">
                    <Form.Control
                      required
                      onChange={this.inputOnChangeHandler}
                      name="password"
                      type="password"
                      placeholder="Password"
                    />
                  </Form.Group>
                  <Form.Group>
                    <Form.Text className="text-danger ">
                      {this.state.validationErrors}
                    </Form.Text>
                    <Form.Text className="text-danger ">
                      {this.state.signupAPIError}
                    </Form.Text>
                  </Form.Group>

                  <Button className="w-100" variant="primary" type="submit">
                    Sign In
                  </Button>
                </Form>
              </div>
              <div className="signup-container">
                <span>Have an account?</span> <Link to="/login">Login In</Link>
              </div>
            </Col>
          </Row>
        </Container>
      );
    }
  }
}
const mapStateToProps = (user) => {
  return { user: user.login };
};
export default connect(mapStateToProps, null)(Signup);
