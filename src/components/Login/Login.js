import React, { Component } from "react";
import { Container, Row, Col, Form, Button } from "react-bootstrap";
import { Link, Redirect } from "react-router-dom";
import { connect } from "react-redux";

import "./login.css";
import loginAction from "../../redux/action/loginAction";
import history from "../../util/history";
class Login extends Component {
  constructor(props) {
    super(props);
    this.usernameRef = React.createRef();
    this.passwordRef = React.createRef();
  }

  formOnSubmitHandler = (event) => {
    event.preventDefault();

    this.props.loginRequest(
      this.usernameRef.current.value,
      this.passwordRef.current.value
    );

    if (this.props.user.user !== null) {
      history.push("/");
    }
  };

  render() {
    console.log(this.props);
    if (this.props.user.user) {
      console.log("redirected render");
      return <Redirect to="/" />;
    } else {
      return (
        <Container className="login-body" fluid>
          <Row className="justify-content-center min-vh-100">
            <Col>
              <div className="login-container">
                <div className="login-heading">
                  <h1>Photogram</h1>
                </div>
                <Form onSubmit={this.formOnSubmitHandler}>
                  <Form.Group controlId="formBasicUsername">
                    <Form.Control
                      ref={this.usernameRef}
                      required
                      type="text"
                      name="username"
                      placeholder="Enter username"
                    />
                  </Form.Group>
                  <Form.Group controlId="formBasicPassword">
                    <Form.Control
                      ref={this.passwordRef}
                      required
                      type="password"
                      name="password"
                      placeholder="Password"
                    />
                  </Form.Group>
                  <Form.Group>
                    <Form.Text className="text-danger ">
                      {this.props.user.error}
                    </Form.Text>
                  </Form.Group>
                  <Button className="w-100" variant="primary" type="submit">
                    Log In
                  </Button>
                </Form>
              </div>
              <div className="login-container">
                <span> Don't have an account?</span>{" "}
                <Link to="/signup"> Sign up</Link>
              </div>
            </Col>
          </Row>
        </Container>
      );
    }
  }
}

const mapStateToProps = (user) => {
  return { user: user.login };
};
const mapDispatchToProps = (dispatch) => {
  return {
    loginRequest: (username, password) => {
      return loginAction.userLogin(username, password)(dispatch);
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);
