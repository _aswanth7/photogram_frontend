import axios from "axios";
import React, { Component } from "react";
import { connect } from "react-redux";

import "./home.css";

class Posts extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userPost: null,
      isLoading: true,
      error: null,
    };
  }
  componentDidUpdate(prevProps, prevState) {
    console.log(this.state, prevState);
    console.log(this.props, prevProps);
    if (
      JSON.stringify(this.state) !== JSON.stringify(prevState) ||
      JSON.stringify(this.props) !== JSON.stringify(prevProps)
    ) {
      axios
        .get(`${process.env.REACT_APP_BACKEND_URL}/user/getposts`, {
          headers: {
            "access-token": this.props.user.user["secret-token"],
          },
        })
        .then((response) => {
          this.setState({
            userPost: response.data,
            isLoading: false,
            error: null,
          });
          console.log(response.data);
        })
        .catch((error) => {
          console.log(error);
          this.setState({
            isLoading: false,
            error,
          });
        });
    }
  }

  componentDidMount() {
    if (this.props.user.user) {
      axios
        .get(`${process.env.REACT_APP_BACKEND_URL}/user/getposts`, {
          headers: {
            "access-token": this.props.user.user["secret-token"],
          },
        })
        .then((response) => {
          this.setState({
            userPost: response.data,
            isLoading: false,
            error: null,
          });
          console.log(response.data);
        })
        .catch((error) => {
          console.log(error);
          this.setState({
            isLoading: false,
            error,
          });
        });
    }
  }

  render() {
    let loader = (
      <div className="lds-spinner">
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
      </div>
    );

    let posts = [];
    this.state.userPost !== null &&
      this.state.userPost.map((post) => {
        posts.push(
          <div style={{ maxWidth: "1024px", margin: " 1em auto" }}>
            <div className="postContainer">
              <div className="post-image">
                <img
                  src={
                    process.env.REACT_APP_BACKEND_URL + "/" + post.post_image
                  }
                ></img>
              </div>
              <div className="post-info">
                <div className="post-info-username">
                  <p>{post.username}</p>
                </div>
                <div className="post-info-comment">
                  <div className="post-info-comment-item">
                    <span>{post.post_caption}</span>
                  </div>
                  <div className="post-info-comment-item">
                    <span>username : comments</span>
                  </div>
                  <div className="post-info-comment-item">
                    <span>username : comments</span>
                  </div>
                  <div className="post-info-comment-item">
                    <span>username : comments</span>
                  </div>
                  <div className="post-info-comment-item">
                    <span>username : comments</span>
                  </div>
                  <div className="post-info-comment-item">
                    <span>username : comments</span>
                  </div>
                  <div className="post-info-comment-item">
                    <span>username : comments</span>
                  </div>
                  <div className="post-info-comment-item">
                    <span>username : comments</span>
                  </div>
                  <div className="post-info-comment-item">
                    <span>username : comments</span>
                  </div>

                  <div className="post-info-comment-item">
                    <span>username : comments</span>
                  </div>
                </div>
                <div className="post-info-details">
                  <div className="post-info-details-like">
                    <p>500</p> <i className="far fa-heart"></i>
                  </div>
                  <div className="post-info-details-comment">
                    <p>20</p> <i className="far fa-comment"></i>
                  </div>
                </div>
                <div className="post-info-comment-input">
                  <input type="text" placeholder="Add comment"></input>
                  <span className="">Post</span>
                </div>
              </div>
            </div>
          </div>
        );
      });

    return this.state.isLoading ? (
      <div className="d-flex justify-content-center">{loader}</div>
    ) : this.state.error ? (
      <div className="d-flex justify-content-center">
        {this.state.error.message}
      </div>
    ) : (
      posts
    );
  }
}
const mapStateToProps = (user) => {
  return { user: user.login };
};
export default connect(mapStateToProps, null)(Posts);
