import React, { Component } from "react";
import NavBar from "../NavBar/NavBar";
import UserPost from "./UserPost";
import UserProfile from "./UserProfile";

import "./profile.css";
import axios from "axios";

class Profile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      posts: null,
      profile: null,
      loading: true,
      error: null,
    };
  }
  componentDidMount() {
    let token = JSON.parse(localStorage.getItem("userInfo"))["secret-token"];
    axios
      .get(`${process.env.REACT_APP_BACKEND_URL}/user/getuserposts`, {
        headers: {
          "access-token": token,
        },
      })
      .then((response) => {
        this.setState({
          posts: response.data,
          loading: false,
          error: null,
        });
        console.log(response.data, "data");
      })
      .catch((error) => {
        console.log(error);
        this.setState({
          posts: null,
          loading: false,
          error,
        });
      });
  }

  render() {
    return (
      <div style={{ maxWidth: "1024px", margin: "0 auto" }}>
        <NavBar />
        <UserProfile />
        <UserPost posts={this.state.posts} />
      </div>
    );
  }
}

export default Profile;
