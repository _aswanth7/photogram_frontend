import React, { Component } from "react";
import { Button } from "react-bootstrap";
import ReactModal from "react-modal";
// import Modal from "./Modal";

class UserPost extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isModalOpen: false,
      selectedImage: null,
    };
  }

  onPostImageClickHandler = (event) => {
    this.setState({
      isModalOpen: true,
      selectedImage: event.target.currentSrc,
    });
  };

  closeModal = (event, modalRef) => {
    // if (event.target === modalRef) {
    this.setState({
      isModalOpen: false,
      selectedImage: null,
    });
    // }
  };

  render() {
    console.log(this.props);
    return (
      <div className="post-container">
        <ReactModal
          isOpen={false}
          shouldCloseOnOverlayClick={true}
          onRequestClose={this.closeModal}
          style={{
            overlay: {
              backgroundColor: " rgba(0,0,0,.8)",
            },
            content: {
              padding: "none",
              background: "none",
              border: "none",
              borderRadius: "none",
              inset: "100px",
            },
          }}
        >
          <div className="modal-container">
            <div className="modalImgContainer">
              <img
                style={{ width: "100%" }}
                src={this.state.selectedImage}
              ></img>
            </div>
            <div className="modalInfoContainer">
              <div>comments</div>
              <div>like</div>
            </div>
          </div>
        </ReactModal>

        <p>
          <i style={{ color: "grey" }} className="fas fa-th"></i> POSTS
        </p>
        <div className="post-grid">
          {this.props.posts &&
            this.props.posts.map((post) => {
              return (
                <div key={post.post_id} className="post-item">
                  <img
                    alt=""
                    src={`${process.env.REACT_APP_BACKEND_URL}/${post.post_image}`}
                    onClick={this.onPostImageClickHandler}
                  ></img>
                </div>
              );
            })}
        </div>
      </div>
    );
  }
}

export default UserPost;
