import React, { useRef } from "react";

const Modal = ({ selectedImage, closeModal }) => {
  const modalRef = useRef(null);

  const modal = {
    display: "flex",
    position: "fixed",
    height: "100vh",
    top: "0px",
    left: "0px",
    backgroundColor: "rgba(0, 0, 0, 0.5)",
    justifyContent: "center",
    alignItems: "center",

    width: "100vw",
  };
  const modalContainer = {
    display: "flex",
    margin: "0 auto",
    width: "50vw",
  };
  const modalImgContainer = {
    width: "60%",
  };
  const modalInfoContainer = {
    display: "flex",
    flexDirection: "column",
    backgroundColor: "white",
    width: "40%",
  };

  return (
    <div
      ref={modalRef}
      onClick={(e) => {
        closeModal(e, modalRef.current);
      }}
      style={modal}
    >
      <div style={modalContainer}>
        <div style={modalImgContainer}>
          <img style={{ width: "100%" }} src={selectedImage}></img>
        </div>
        <div style={modalInfoContainer}>
          <div>comments</div>
          <div>like</div>
        </div>
      </div>
    </div>
  );
};

export default Modal;
