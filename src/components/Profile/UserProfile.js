import React, { Component } from "react";
import { Link } from "react-router-dom";

class UserProfile extends Component {
  render() {
    let username = JSON.parse(localStorage.getItem("userInfo"))["username"];
    return (
      <div className="profile-container">
        <div className="profile-user-avatar">
          <img src="https://www.pngitem.com/pimgs/m/150-1503945_transparent-user-png-default-user-image-png-png.png"></img>
        </div>
        <div className="profile-user-info">
          <div className="profile-user-info-1">
            <h3>{username}</h3>
            <div>
              <Link>Edit Profile</Link>
            </div>
          </div>
          <div className="profile-user-info-2">
            <p>
              <span>0</span>Post
            </p>
            <p>
              <span>465</span>Followers
            </p>
            <p>
              <span>502</span>Following
            </p>
          </div>
          <div className="profile-user-info-3">
            <p>Aswanth Prabhakaran</p>
            <span>
              Lorem Ipsum is simply dummy text of the printing and typesetting
              industry.
            </span>
          </div>
        </div>
      </div>
    );
  }
}

export default UserProfile;
