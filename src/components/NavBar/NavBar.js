import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import { Modal, Button, Form, Dropdown } from "react-bootstrap";
import "./navbar.css";

import axios from "axios";
import { connect } from "react-redux";
import loginAction from "../../redux/action/loginAction";

class NavBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: false,
      result: null,
      error: null,
    };
    this.imageRef = React.createRef();
    this.captionRef = React.createRef();
  }
  handleClose = (event) => {
    this.setState({
      isOpen: false,
    });
  };
  openModal = (event) => {
    this.setState({
      isOpen: true,
    });
  };
  onFormSubmit = async (event) => {
    event.preventDefault();
    try {
      const data = new FormData();

      data.append("image", this.imageRef.current.files[0]);
      data.append("caption", this.captionRef.current.value);
      data.append("username", this.props.user.user.username);

      const formData = new FormData();
      formData.append("file", this.state.pictureAsFile);

      let result = await axios.post(
        process.env.REACT_APP_BACKEND_URL + "/user/addpost",
        data,
        {
          headers: {
            "Content-Type": "application/json",
            "access-token": this.props.user.user["secret-token"],
          },
        }
      );
      this.setState({
        result: result.data.message,
        error: null,
      });
    } catch (error) {
      console.log(error);
      this.setState({
        error,
        result: null,
      });
    }
  };

  render() {
    return (
      <div>
        <nav className="home-nav">
          <div>
            <h3>Photogram</h3>
          </div>
          <div>
            <form className="home-nav-form">
              <input
                type="text"
                placeholder="&#xf002; Search"
                style={{ fontFamily: " FontAwesome" }}
              ></input>
            </form>
          </div>

          <div className="home-nav-container">
            <NavLink
              className="home-nav-item addpost"
              onClick={this.openModal}
              to="#"
            >
              <i className="far fa-plus-square"></i>
            </NavLink>
            <NavLink className="home-nav-item" to="/">
              <i className="fa fa-home"></i>
            </NavLink>
            <NavLink className="home-nav-item" to="#">
              <i className="fab fa-facebook-messenger"></i>
            </NavLink>
            <Dropdown>
              <Dropdown.Toggle variant="light" id="dropdown-basic">
                <div className="home-nav-item-icon">
                  <i className="fa fa-user-circle"></i>
                </div>
              </Dropdown.Toggle>

              <Dropdown.Menu>
                <Dropdown.Item>
                  <NavLink className="home-nav-dropdown-item" to="/profile">
                    Profile
                  </NavLink>
                </Dropdown.Item>
                <Dropdown.Item
                  onClick={() => {
                    this.props.logout();
                  }}
                >
                  <NavLink className="home-nav-dropdown-item" to="#">
                    Logout
                  </NavLink>
                </Dropdown.Item>
              </Dropdown.Menu>
            </Dropdown>
          </div>
        </nav>
        <Modal show={this.state.isOpen} onHide={this.handleClose}>
          <Modal.Header closeButton>
            <Modal.Title>Add Post</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            {this.state.error && (
              <div className="text-center text-danger">Post Failed</div>
            )}
            {this.state.result && (
              <div className="text-center text-success">
                {this.state.result}
              </div>
            )}
            <Form encType="multipart/form-data">
              <Form.Group controlId="formGroupEmail">
                <Form.Label>Upload Image</Form.Label>
                <Form.Control type="file" ref={this.imageRef} />
              </Form.Group>
              <Form.Group controlId="formGroupPassword">
                <Form.Label>Caption</Form.Label>
                <Form.Control as="textarea" ref={this.captionRef} rows={3} />
              </Form.Group>
            </Form>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={this.handleClose}>
              Close
            </Button>
            <Button variant="primary" onClick={this.onFormSubmit} type="submit">
              Post
            </Button>
          </Modal.Footer>
        </Modal>
      </div>
    );
  }
}
const mapStateToProps = (user) => {
  return { user: user.login };
};

const mapDispatchToProps = (dispatch) => {
  return {
    logout: () => {
      return loginAction.userLogout()(dispatch);
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(NavBar);
